import React from 'react';
import ReactDOM from 'react-dom';
import PrismicApp from './PrismicApp';
import './index.scss';

ReactDOM.render(
  <PrismicApp />,
  document.getElementById('root'),
);
